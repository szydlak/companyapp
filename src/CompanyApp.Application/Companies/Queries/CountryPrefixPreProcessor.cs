﻿using CompanyApp.Application.Helpers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CompanyApp.Application.Companies.Queries
{
    public class CountryPrefixPreProcessor : IRequestProcessor<GetCompanyQuery, CompanyPreview>
    {
        public Task Process(GetCompanyQuery request, CancellationToken cancellationToken)
        {
            var pattern = new Regex(@"^[a-zA-Z]{2}[0-9]{10}");

            if (pattern.IsMatch(request.SearchValue))
            {
                var searchValue = request.SearchValue.Remove(0, 2);
                request.SearchValue = searchValue;
            }

            return Task.CompletedTask;
        }
    }
}