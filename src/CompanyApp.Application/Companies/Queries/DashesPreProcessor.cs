﻿using CompanyApp.Application.Helpers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CompanyApp.Application.Companies.Queries
{
    public class DashesPreProcessor : IRequestProcessor<GetCompanyQuery, CompanyPreview>
    {
        public Task Process(GetCompanyQuery request, CancellationToken cancellationToken)
        {
            var pattern = new Regex(@"^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$");
            if (pattern.IsMatch(request.SearchValue))
            {
                request.SearchValue = request.SearchValue.Replace("-", string.Empty);
            }

            return Task.CompletedTask;
        }
    }
}