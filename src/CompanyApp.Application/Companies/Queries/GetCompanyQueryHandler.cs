﻿using CompanyApp.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CompanyApp.Application.Companies.Queries
{
    public class GetCompanyQueryHandler : IRequestHandler<GetCompanyQuery, CompanyPreview>
    {
        private readonly ApplicationDbContext _context;

        public GetCompanyQueryHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<CompanyPreview> Handle(GetCompanyQuery request, CancellationToken cancellationToken)
        {
            var company = await _context.Companies
                .Where(n => n.Nip == request.SearchValue || n.Krs == request.SearchValue || n.Regon == request.SearchValue)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var result = CompanyPreview.Create(company);
            return result;
        }
    }
}