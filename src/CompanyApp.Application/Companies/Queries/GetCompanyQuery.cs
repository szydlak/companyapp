﻿using MediatR;

namespace CompanyApp.Application.Companies.Queries
{
    public class GetCompanyQuery : IRequest<CompanyPreview>
    {
        public string SearchValue { get; set; }
    }
}