﻿using CompanyApp.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace CompanyApp.Application.Companies.Queries
{
    public class CompanyPreview
    {
        public string Name { get; set; } = string.Empty;
        public string City { get; set; } = string.Empty;
        public string PostCode { get; set; } = string.Empty;
        public string Street { get; set; } = string.Empty;
        public string Number { get; set; } = string.Empty;

        public static Expression<Func<Company, CompanyPreview>> Projection
        {
            get
            {
                return c => new CompanyPreview
                {
                    City = c.City,
                    Name = c.Name,
                    Number = c.Number,
                    PostCode = c.PostCode,
                    Street = c.Street
                };
            }
        }

        public static CompanyPreview Create(Company company)
        {
            return company is null ? new CompanyPreview() : Projection.Compile().Invoke(company);
        }
    }
}