﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CompanyApp.Application.Helpers
{
    public class RequestPreProcessorBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse> where TResponse : class
    {
        private readonly IEnumerable<IRequestProcessor<TRequest, TResponse>> _preProcessors;

        public RequestPreProcessorBehavior(IEnumerable<IRequestProcessor<TRequest, TResponse>> preProcessors)
        {
            _preProcessors = preProcessors;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            foreach (var processor in _preProcessors)
            {
                await processor.Process(request, cancellationToken);
            }
            return await next().ConfigureAwait(false);
        }
    }
}