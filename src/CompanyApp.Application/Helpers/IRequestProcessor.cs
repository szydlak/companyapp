﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CompanyApp.Application.Helpers
{
    public interface IRequestProcessor<TRequest, TResponse> where TRequest : IRequest<TResponse> where TResponse : class
    {
        Task Process(TRequest request, CancellationToken cancellationToken);
    }
}