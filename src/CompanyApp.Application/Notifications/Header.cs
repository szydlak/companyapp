﻿namespace CompanyApp.Application.Notifications
{
    public class HeaderDto
    {
        public HeaderDto(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; }
        public string Value { get; }
    }
}