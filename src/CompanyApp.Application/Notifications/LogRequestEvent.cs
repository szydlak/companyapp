﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CompanyApp.Application.Notifications
{
    public class LogRequestEvent : INotification
    {
        public DateTime Date { get; }
        public string Path { get; }
        private readonly IList<HeaderDto> _headers = new List<HeaderDto>();

        public LogRequestEvent(string path) : this()
        {
            Date = DateTime.Now;
            Path = path;
        }

        protected LogRequestEvent() => Headers = new ReadOnlyCollection<HeaderDto>(_headers);

        public IEnumerable<HeaderDto> Headers { get; }

        public void AddHeader(HeaderDto header)
        {
            if (header != null)
                _headers.Add(header);
        }
    }
}