﻿using CompanyApp.Domain.Entities;
using CompanyApp.Persistence;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CompanyApp.Application.Notifications
{
    public class LogRequestEventHandler : INotificationHandler<LogRequestEvent>
    {
        private readonly ApplicationDbContext _context;

        public LogRequestEventHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(LogRequestEvent notification, CancellationToken cancellationToken)
        {
            var log = new Log
            {
                Date = notification.Date,
                Path = notification.Path
            };

            foreach (var header in notification.Headers)
            {
                log.Headers.Add(new Header
                {
                    Name = header.Name,
                    Value = header.Value
                });
            }

            _context.Logs.Add(log);
            await _context.SaveChangesAsync();
        }
    }
}