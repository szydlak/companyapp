﻿namespace CompanyApp.Domain.Entities
{
    public class Header
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public long LogId { get; set; }
        public Log Log { get; set; }
    }
}