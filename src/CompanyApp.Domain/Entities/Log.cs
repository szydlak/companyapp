﻿using System;
using System.Collections.Generic;

namespace CompanyApp.Domain.Entities
{
    public class Log
    {
        public long Id { get; set; }
        public string Path { get; set; }
        public DateTime Date { get; set; }
        public ICollection<Header> Headers { get; set; }

        public Log()
        {
            Headers = new HashSet<Header>();
        }
    }
}