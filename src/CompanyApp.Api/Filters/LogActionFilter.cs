﻿using CompanyApp.Application.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyApp.Api.Filters
{
    public class LogActionFilter : IAsyncActionFilter
    {
        private readonly IMediator _mediator;

        public LogActionFilter(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logRequestEvent = MakeLogRequest(context);
            await _mediator.Publish(logRequestEvent);
            await next.Invoke();
        }

        private LogRequestEvent MakeLogRequest(ActionExecutingContext context)
        {
            var logRequestEvent = new LogRequestEvent(context.HttpContext.Request.Path);
            var headers = context.HttpContext.Request.Headers.Select(d => new HeaderDto(d.Key, d.Value)).ToList();
            foreach (var header in headers)
            {
                logRequestEvent.AddHeader(header);
            }
            return logRequestEvent;
        }
    }
}