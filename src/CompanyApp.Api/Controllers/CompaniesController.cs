﻿using CompanyApp.Api.Filters;
using CompanyApp.Application.Companies.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CompanyApp.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly IMediator _mediator;

        // GET api/values
        public CompaniesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [ServiceFilter(typeof(LogActionFilter))]
        [HttpGet("{nip}")]
        public async Task<IActionResult> Get(string nip)
        {
            var result = await _mediator.Send(new GetCompanyQuery { SearchValue = nip });
            return Ok(result);
        }
    }
}