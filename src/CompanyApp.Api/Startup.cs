﻿using CompanyApp.Api.Extensions;
using CompanyApp.Api.Filters;
using CompanyApp.Application.Companies.Queries;
using CompanyApp.Application.Helpers;
using CompanyApp.Persistence;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace CompanyApp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<ApplicationDbContext>(options => options
            .UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]), ServiceLifetime.Transient);
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(Application.Helpers.RequestPreProcessorBehavior<,>));
            services.RegisterAllTypes(typeof(CountryPrefixPreProcessor).Assembly, typeof(IRequestProcessor<,>));
            services.AddMediatR(typeof(GetCompanyQueryHandler).GetTypeInfo().Assembly);
            services.AddScoped<LogActionFilter>();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                        builder.WithOrigins("http://localhost:3000")
                               .AllowAnyHeader()
                               .AllowAnyMethod());

            app.UseMvc();
        }
    }
}