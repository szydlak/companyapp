﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CompanyApp.Persistence.Migrations
{
    public partial class Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    Nip = table.Column<string>(nullable: true),
                    Regon = table.Column<string>(nullable: true),
                    Krs = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "City", "Krs", "Name", "Nip", "Number", "PostCode", "Regon", "Street" },
                values: new object[] { 1, "WARSZAWA", "0000059944", "AGORA SPÓŁKA AKCYJNA", "5260305644", "8/10", "00-732", "011559486", "CZERSKA" });

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "City", "Krs", "Name", "Nip", "Number", "PostCode", "Regon", "Street" },
                values: new object[] { 2, "RZESZÓW", "0000033391", "ASSECO POLAND SPÓŁKA AKCYJNA", "5220003782", "14", "35-322", "010334578", "OLCHOWA" });

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "City", "Krs", "Name", "Nip", "Number", "PostCode", "Regon", "Street" },
                values: new object[] { 3, "WARSZAWA", "0000025237", "MBANK SPÓŁKA AKCYJNA", "5260215088", "18", "00-950", "001254524", "SENATORSKA" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}