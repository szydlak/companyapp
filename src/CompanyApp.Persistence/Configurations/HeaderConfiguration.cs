﻿using CompanyApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CompanyApp.Persistence.Configurations
{
    public class HeaderConfiguration : IEntityTypeConfiguration<Header>
    {
        public void Configure(EntityTypeBuilder<Header> builder)
        {
            builder.HasKey(k => k.Id);
            builder.HasOne(d => d.Log)
                .WithMany(d => d.Headers);
        }
    }
}