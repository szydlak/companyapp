﻿using CompanyApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CompanyApp.Persistence.Configurations
{
    public class LogConfiguration : IEntityTypeConfiguration<Log>
    {
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.HasKey(k => k.Id);
            builder.HasMany(d => d.Headers)
                .WithOne(d => d.Log)
                .HasForeignKey(d => d.LogId);
        }
    }
}