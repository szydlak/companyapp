﻿using CompanyApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CompanyApp.Persistence.Configurations
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(k => k.Id);
            builder.HasData(
                new
                {
                    Id = 1,
                    Name = "AGORA SPÓŁKA AKCYJNA",
                    City = "WARSZAWA",
                    PostCode = "00-732",
                    Street = "CZERSKA",
                    Number = "8/10",
                    Nip = "5260305644",
                    Regon = "011559486",
                    Krs = "0000059944"
                },
                new
                {
                    Id = 2,
                    Name = "ASSECO POLAND SPÓŁKA AKCYJNA",
                    City = "RZESZÓW",
                    PostCode = "35-322",
                    Street = "OLCHOWA",
                    Number = "14",
                    Nip = "5220003782",
                    Regon = "010334578",
                    Krs = "0000033391"
                },
                new
                {
                    Id = 3,
                    Name = "MBANK SPÓŁKA AKCYJNA",
                    City = "WARSZAWA",
                    PostCode = "00-950",
                    Street = "SENATORSKA",
                    Number = "18",
                    Nip = "5260215088",
                    Regon = "001254524",
                    Krs = "0000025237"
                });
        }
    }
}