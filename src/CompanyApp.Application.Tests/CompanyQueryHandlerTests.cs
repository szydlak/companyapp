﻿using CompanyApp.Application.Companies.Queries;
using CompanyApp.Domain.Entities;
using CompanyApp.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CompanyApp.Application.Tests
{
    public class CompanyQueryHandlerTests
    {
        [Theory]
        [InlineData("5260305644")]
        [InlineData("010334578")]
        [InlineData("0000025237")]
        public async Task CallQueryHandlerShouldReturnCompanyPreviewWithData(string searchValue)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var _context = new ApplicationDbContext(options))
            {

                _context.Companies.AddRange(this.GetSampleCompanies());
                var expectedResult = GetSampleCompanies().Where(c => c.Nip == searchValue || c.Regon == searchValue || c.Krs == searchValue).FirstOrDefault();
                await _context.SaveChangesAsync();
                var query = new GetCompanyQuery
                {
                    SearchValue = searchValue
                };
                var handler = new GetCompanyQueryHandler(_context);

                var result = await handler.Handle(query, CancellationToken.None);
                Assert.Equal(result.City, expectedResult.City);
                Assert.Equal(result.PostCode, expectedResult.PostCode);
                Assert.Equal(result.Street, expectedResult.Street);
                Assert.Equal(result.Number, expectedResult.Number);
                Assert.Equal(result.Name, expectedResult.Name);
            }
        }

        [Fact]
        public async Task CallQueryHandlerShouldReturnCompanyPreviewWithEmptyData()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            using (var _context = new ApplicationDbContext(options))
            {

                string searchValue = "1243454123";
                _context.Companies.AddRange(this.GetSampleCompanies());
                await _context.SaveChangesAsync();

                var query = new GetCompanyQuery
                {
                    SearchValue = searchValue
                };

                var handler = new GetCompanyQueryHandler(_context);
                var result = await handler.Handle(query, CancellationToken.None);

                var expectedResult = new CompanyPreview();
                Assert.Equal(expectedResult.City, result.City);
            }
        }


        private IEnumerable<Company> GetSampleCompanies()
        {
            List<Company> list = new List<Company>
            {
                new Company {
                        Id = 1,
                        Name = "AGORA SPÓŁKA AKCYJNA",
                        City = "WARSZAWA",
                        PostCode = "00-732",
                        Street = "CZERSKA",
                        Number = "8/10",
                        Nip = "5260305644",
                        Regon = "011559486",
                        Krs = "0000059944"
                },
                new Company
                    {
                        Id = 2,
                        Name = "ASSECO POLAND SPÓŁKA AKCYJNA",
                        City = "RZESZÓW",
                        PostCode = "35-322",
                        Street = "OLCHOWA",
                        Number = "14",
                        Nip = "5220003782",
                        Regon = "010334578",
                        Krs = "0000033391"
                    },
                new Company
                    {
                        Id = 3,
                        Name = "MBANK SPÓŁKA AKCYJNA",
                        City = "WARSZAWA",
                        PostCode = "00-950",
                        Street = "SENATORSKA",
                        Number = "18",
                        Nip = "5260215088",
                        Regon = "001254524",
                        Krs = "0000025237"
                    }
            };
            return list;

        }
    }
}
