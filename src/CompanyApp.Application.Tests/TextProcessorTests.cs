﻿using CompanyApp.Application.Companies.Queries;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CompanyApp.Application.Tests
{
    public class TextProcessorTests
    {
        [Fact]
        public async Task ShouldHaveNoLines()
        {
            var query = new GetCompanyQuery
            {
                SearchValue = "111-111-11-11"
            };

            var textProcessor = new DashesPreProcessor();
            await textProcessor.Process(query, CancellationToken.None);
            Assert.Equal("1111111111", query.SearchValue);
        }

        [Fact]
        public async Task ShouldHaveNoPrefix()
        {
            var query = new GetCompanyQuery
            {
                SearchValue = "PL1111111111"
            };

            var textProcessor = new CountryPrefixPreProcessor();
            await textProcessor.Process(query, CancellationToken.None);
            Assert.Equal("1111111111", query.SearchValue);
        }
    }
}
